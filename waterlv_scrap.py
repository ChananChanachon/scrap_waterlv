import os
import selenium
from selenium import webdriver
import time
import io
import requests
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import ElementClickInterceptedException

from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import pandas as pd
from datetime import datetime

path = os.getcwd() + '/'
url = 'http://www.thaiwater.net/water/wl'
path_to_driver = os.getcwd() + '/chromedriver'

df_stack =  []

options = Options()
options.add_argument("--headless")
browser = webdriver.Chrome(executable_path = path_to_driver, chrome_options=options)
browser.get(url)

time.sleep(6)

drop_page = browser.find_element_by_xpath('//*[@class="MuiSelect-root MuiSelect-select MuiTablePagination-select MuiSelect-selectMenu MuiInputBase-input"]')
drop_page.click()
time.sleep(3)

select_100 = browser.find_element_by_xpath('//*[@data-value="100"]')
select_100.click()
time.sleep(3)

data = browser.find_elements_by_xpath('//*[@class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-12 MuiGrid-grid-md-12 MuiGrid-grid-lg-12"]')
time.sleep(3)

text = data[0].text
txt_split = text.split('\n')[:-4]

columns_name = []
for num,value in enumerate(txt_split[:2]):
    columns_name.append(value)
columns_name.append(txt_split[2]+'_'+txt_split[3])
columns_name.append(txt_split[4]+'_'+txt_split[5])
for i in txt_split[6][2:].split(' '):
    columns_name.append(i)

range_num = [k for k in range(0,int(len(text.split('\n')[7:-4]))+1,4)]

rows = []
for num,value in enumerate(range_num):
    if value != int(len(text.split('\n')[7:-4])):
        row = txt_split[7:][range_num[num]:range_num[num+1]]
        
        fix = []
        ### STATION
        fix.append(row[0])
        
        ### PLACE
        row[1] = row[1].split()
        place = row[1][:3]
        separator = ', '
        pl = separator.join(place)
        
        fix.append(pl)
        
        ### VALUE
        for n,v in enumerate(row[1][3:]):
            fix.append(v)
            
        ### STATUS
        fix.append(row[2])
        
        ### TIME
        fix.append(row[3])
        
        rows.append(fix)
    else:
        pass
    
df = pd.DataFrame(rows, columns = columns_name)
df_stack.append(df)

click = browser.find_element_by_xpath('//*[@d="M5.59 7.41L10.18 12l-4.59 4.59L7 18l6-6-6-6zM16 6h2v12h-2z"]')
click.click()
time.sleep(3)

data = browser.find_elements_by_xpath('//*[@class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-12 MuiGrid-grid-md-12 MuiGrid-grid-lg-12"]')
time.sleep(3)

text = data[0].text
txt_split = text.split('\n')[:-4]

range_num = [k for k in range(0,int(len(text.split('\n')[7:-4]))+1,4)]

columns_name = []
for num,value in enumerate(txt_split[:2]):
    columns_name.append(value)
columns_name.append(txt_split[2]+'_'+txt_split[3])
columns_name.append(txt_split[4]+'_'+txt_split[5])
for i in txt_split[6][2:].split(' '):
    columns_name.append(i)
    
rows = []
for num,value in enumerate(range_num):
    if value != int(len(text.split('\n')[7:-4])):
        row = txt_split[7:][range_num[num]:range_num[num+1]]
        
        fix = []
        ### STATION
        fix.append(row[0])
        
        ### PLACE
        row[1] = row[1].split()
        place = row[1][:3]
        separator = ', '
        pl = separator.join(place)
        
        fix.append(pl)
        
        ### VALUE
        for n,v in enumerate(row[1][3:]):
            fix.append(v)
            
        ### STATUS
        fix.append(row[2])
        
        ### TIME
        fix.append(row[3])
        
        rows.append(fix)
    else:
        pass
    
df = pd.DataFrame(rows, columns = columns_name)
df_stack.append(df)

for i in range(2):
    click = browser.find_element_by_xpath('//*[@d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"]')
    click.click()
    time.sleep(3)
    
    data = browser.find_elements_by_xpath('//*[@class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-12 MuiGrid-grid-md-12 MuiGrid-grid-lg-12"]')
    time.sleep(3)
    
    text = data[0].text
    txt_split = text.split('\n')[:-4]
    
    range_num = [k for k in range(0,int(len(text.split('\n')[7:-4]))+1,4)]
    
    columns_name = []
    for num,value in enumerate(txt_split[:2]):
        columns_name.append(value)
    columns_name.append(txt_split[2]+'_'+txt_split[3])
    columns_name.append(txt_split[4]+'_'+txt_split[5])
    for i in txt_split[6][2:].split(' '):
        columns_name.append(i)

    rows = []
    for num,value in enumerate(range_num):
        if value != int(len(text.split('\n')[7:-4])):
            row = txt_split[7:][range_num[num]:range_num[num+1]]

            fix = []
            ### STATION
            fix.append(row[0])

            ### PLACE
            row[1] = row[1].split()
            place = row[1][:3]
            separator = ', '
            pl = separator.join(place)

            fix.append(pl)

            ### VALUE
            for n,v in enumerate(row[1][3:]):
                fix.append(v)

            ### STATUS
            fix.append(row[2])

            ### TIME
            fix.append(row[3])

            rows.append(fix)
        else:
            pass

    df = pd.DataFrame(rows, columns = columns_name)
    df_stack.append(df)
    
result = pd.concat(df_stack)
result['date'] = str(datetime.now().year) + '-0' + str(datetime.now().month) + '-0' + str(datetime.now().day)
result['date'] = pd.to_datetime(result['date'], format = '%Y-%m-%d')

result = result.reset_index().drop(columns = ['index'])

result.to_csv('ข้อมูลระดับน้ำของประเทศไทย.csv',index = False, encoding = 'utf-8-sig')

check = pd.read_csv('water_lv.csv',encoding = 'utf-8')

check = check.rename(columns = {'สถานี':'station','ที่ตั้ง':'address','ระดับน้ำ_(ม.รทก)':'water_level'
                        ,'ระดับตลิ่ง_(ม.รทก)':'water_bank_level','ความจุลำน้ำ':'river_capacity'
                        ,'สถานการณ์น้ำ':'status','เวลา':'data_time'})

check['province'] = 'x'
check['data_time'] = check['data_time'].fillna('x')

for num,value in enumerate(check['address']):
    check.loc[num,'province'] = value.split(', ')[-1].replace('จ.','')
    
    if check['data_time'].iloc[num] == 'x':
        check.loc[num,'data_time'] = check['status'].iloc[num]
        check.loc[num,'status'] = check['river_capacity'].iloc[num]
        check.loc[num,'river_capacity'] = float('nan')
        
check = check[['date','station','province','address','water_level','water_bank_level','river_capacity'
               ,'status','data_time']]

check[check.columns[1]] = check[check.columns[1]].astype(str)
check[check.columns[2]] = check[check.columns[2]].astype(str)
check[check.columns[3]] = check[check.columns[3]].astype(str)
check[check.columns[4]] = check[check.columns[4]].astype(float)
check[check.columns[5]] = check[check.columns[5]].astype(float)
check[check.columns[6]] = check[check.columns[6]].astype(float)
check[check.columns[7]] = check[check.columns[7]].astype(str)
check[check.columns[8]] = check[check.columns[8]].astype(str)

check.to_csv('ข้อมูลระดับน้ำของประเทศไทย.csv', index = 'False',encoding = 'utf-8-sig')